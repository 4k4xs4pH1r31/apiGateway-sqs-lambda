resource "aws_iam_role" "apiSQS" {
  name               = "apigateway_sqs"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    yor_trace = "a12bffa1-250f-4b2c-aeee-3163cd335148"
  }
}

resource "aws_iam_role" "apiLambda" {
  name               = "apigateway_Lambda"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    yor_trace = "4fb637af-09ad-4b35-8c2c-aac04d1d05bf"
  }
}

data "template_file" "gateway_policy" {
  template = file("policies/api-gateway-permission.json")

  vars = {
    sqs_arn1 = aws_sqs_queue.queue.arn
    sqs_arn2 = aws_sqs_queue.i2c_webhook_routing_error_queue.arn
  }
}

resource "aws_iam_policy" "api_policy" {
  name   = "api-sqs-cloudwatch-policy"
  policy = data.template_file.gateway_policy.rendered

  tags = {
    yor_trace = "6da40980-344c-4fac-91ea-df4691576198"
  }
}

resource "aws_iam_role_policy_attachment" "api_exec_role" {
  role       = aws_iam_role.apiSQS.name
  policy_arn = aws_iam_policy.api_policy.arn
}

resource "aws_iam_role_policy_attachment" "api_lambda_role" {
  role       = aws_iam_role.apiLambda.name
  policy_arn = aws_iam_policy.api_policy.arn
}

data "template_file" "lambda_policy" {
  template = file("policies/lambda-permission.json")

  vars = {
    sqs_arn = aws_sqs_queue.queue.arn
  }
}

resource "aws_iam_policy" "lambda_sqs_policy" {
  name        = "lambda_policy_db"
  description = "IAM policy for lambda Being invoked by SQS"

  policy = data.template_file.lambda_policy.rendered

  tags = {
    yor_trace = "c59bfa12-c811-48b1-8877-28db71fba1aa"
  }
}

resource "aws_iam_role" "lambda_exec_role" {
  name               = "${var.name}-lambda-db"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    yor_trace = "f21a4443-aa88-4b44-8ff8-973c75557405"
  }
}

resource "aws_iam_role_policy_attachment" "lambda_role_policy" {
  role       = aws_iam_role.lambda_exec_role.name
  policy_arn = aws_iam_policy.lambda_sqs_policy.arn
}

data "aws_iam_policy_document" "assume_role" {
  count = var.enabled ? 1 : 0

  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = concat(slice(["lambda.amazonaws.com", "edgelambda.amazonaws.com"], 0, var.lambda_at_edge ? 2 : 1), var.trusted_entities)
    }
  }
}

resource "aws_iam_role" "lambda" {
  count = var.enabled ? 1 : 0

  name               = var.function_name
  assume_role_policy = data.aws_iam_policy_document.assume_role[0].json

  tags = merge(var.tags, {
    yor_trace = "67ebf917-8555-4f76-86b2-fd6678defece"
  })
}

locals {
  lambda_log_group_arn      = "arn:${data.aws_partition.current.partition}:logs:*:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${var.function_name}"
  lambda_edge_log_group_arn = "arn:${data.aws_partition.current.partition}:logs:*:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/us-east-1.${var.function_name}"
  log_group_arns            = slice([local.lambda_log_group_arn, local.lambda_edge_log_group_arn], 0, var.lambda_at_edge ? 2 : 1)
}

data "aws_iam_policy_document" "logs" {
  count = var.enabled && var.cloudwatch_logs ? 1 : 0

  statement {
    sid    = "1"
    effect = "Allow"
    actions = [
      "s3:*"
    ]

    resources = [
      "foo",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = concat(formatlist("%v:*", local.log_group_arns), formatlist("%v:*:*", local.log_group_arns))
  }
}

resource "aws_iam_policy" "logs" {
  count = var.enabled && var.cloudwatch_logs ? 1 : 0

  name   = "${var.function_name}-logs"
  policy = data.aws_iam_policy_document.logs[0].json

  tags = {
    yor_trace = "e7964da8-309f-4f33-a97d-cfc3397e8dab"
  }
}

resource "aws_iam_policy_attachment" "logs" {
  count = var.enabled && var.cloudwatch_logs ? 1 : 0

  name       = "${var.function_name}-logs"
  roles      = [aws_iam_role.lambda[0].name]
  policy_arn = aws_iam_policy.logs[0].arn
}

data "aws_iam_policy_document" "dead_letter" {
  count = var.dead_letter_config == null ? 0 : var.enabled ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "sns:Publish",
      "sqs:SendMessage",
    ]

    resources = [
      var.dead_letter_config.target_arn,
    ]
  }
}

resource "aws_iam_policy" "dead_letter" {
  count = var.dead_letter_config == null ? 0 : var.enabled ? 1 : 0

  name   = "${var.function_name}-dl"
  policy = data.aws_iam_policy_document.dead_letter[0].json

  tags = {
    yor_trace = "587caa7f-ec19-4c2b-a627-3bfd62cd2a26"
  }
}

resource "aws_iam_policy_attachment" "dead_letter" {
  count = var.dead_letter_config == null ? 0 : var.enabled ? 1 : 0

  name       = "${var.function_name}-dl"
  roles      = [aws_iam_role.lambda[0].name]
  policy_arn = aws_iam_policy.dead_letter[0].arn
}

data "aws_iam_policy_document" "network" {
  count = var.vpc_config == null ? 0 : var.enabled ? 1 : 0

  statement {
    sid    = "1"
    effect = "Allow"
    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface",
    ]

    resources = [
      "foo",
    ]
  }
}

resource "aws_iam_policy" "network" {
  count = var.vpc_config == null ? 0 : var.enabled ? 1 : 0

  name   = "${var.function_name}-network.arn"
  policy = data.aws_iam_policy_document.network[0].json

  tags = {
    yor_trace = "cb4da427-c65f-4b09-920d-d364f676c409"
  }
}

resource "aws_iam_policy_attachment" "network" {
  count = var.vpc_config == null ? 0 : var.enabled ? 1 : 0

  name       = "${var.function_name}-network"
  roles      = [aws_iam_role.lambda[0].name]
  policy_arn = aws_iam_policy.network[0].arn
}

resource "aws_iam_policy" "additional" {
  count = var.policy == null ? 0 : var.enabled ? 1 : 0

  name   = var.function_name
  policy = var.policy.json

  tags = {
    yor_trace = "106c6914-aa23-42a5-9e3f-edf1f0c1ef03"
  }
}

resource "aws_iam_policy_attachment" "additional" {
  count = var.policy == null ? 0 : var.enabled ? 1 : 0

  name       = var.function_name
  roles      = [aws_iam_role.lambda[0].name]
  policy_arn = aws_iam_policy.additional[0].arn
}
