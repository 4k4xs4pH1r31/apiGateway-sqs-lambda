# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/aws" {
  version     = "5.43.0"
  constraints = "5.43.0"
  hashes = [
    "h1:9l6juBAwKzO2AdL+SS2J3B1q4s8DpAzHLMb0Jbsp5VM=",
    "zh:2920f88c31ef2733737d2370ce42c237205274f37fd02deaaff6357f6faeb731",
    "zh:3cf8766f2de846932fe3b90e17dd2d6eff96cb0acb600420a1c139c68001e44a",
    "zh:4a6b7a024fcf98a622bc64ec565df19bfac93c612942e90b2a321c86ee8e8f24",
    "zh:756d129653c4e4c88537a635f2f3bc34ab6fa91827406a1efda52e502f057c75",
    "zh:8b23287ebf67e1f32647a11933519da7895b1f4970d1ff58cd7b06f5eb1c42c4",
    "zh:9a423b09ce6680ff5ff655c908977ec30cc44eedee7e2e3a09cc55af3f9edc4c",
    "zh:aeb593ce863fa4b97b891a3772c660200e2d55b38eb152f92f377a3402fa7e76",
    "zh:cb3c7a578a2ab507ac74f819f831afcbe343441eae26e8f77727c9597afe827d",
    "zh:de9eaaf0f8ee2e2116ff664ceefefae97b93f14ce2103494b95f1d59e4ce60d7",
    "zh:e27cb09ebc09a8327ccf1c4b2c27a1fcaebbdb3da15cac0aeff10b1f9e436cd7",
  ]
}

provider "registry.opentofu.org/hashicorp/external" {
  version = "2.3.3"
  hashes = [
    "h1:bDJy8Mj5PMTEuxm6Wu9A9dATBL+mQDmHx8NnLzjvCcc=",
    "zh:1ec36864a1872abdfd1c53ba3c6837407564ac0d86ab80bf4fdc87b41106fe68",
    "zh:2117e0edbdc88f0d22fe02fe6b2cfbbbc5d5ce40f8f58e484d8d77d64dd7340f",
    "zh:4bcfdacd8e2508c16e131de9072cecd359e0ade3b8c6798a049883f37a5872ea",
    "zh:4da71bc601a37bf8b7413c142d43f5f28e97e531d4836ee8624f41b9fb62e250",
    "zh:55b9eebac79a46f88db5615f1ee0ac4c3f9351caa4eb8542171ef5d87de60338",
    "zh:74d64afaef190321f8ddf1c4a9c6489d6cf51098704a2456c1553406e8306328",
    "zh:8a357e51a0ec69872fafc64da3c6a1039277d325255ef5a264b727d83995d18b",
    "zh:aacd2e6c13fe19115d51cd28a40a28da017bb48c2e18dec4460d1c37506b1495",
    "zh:e19c8bdf0e059341d008a50f9138c44009e9ebb3a8047a300e6bc63ed8af8ea0",
    "zh:fafa9639d8b8402e35f3864c6cfb0762ec57cc365a8f383e2acf81105b1b9eea",
  ]
}

provider "registry.opentofu.org/hashicorp/null" {
  version = "3.2.2"
  hashes = [
    "h1:xN1tSeF/rUBfaddk/AVqk4i65z/MMM9uVZWd2cWCCH0=",
    "zh:00e5877d19fb1c1d8c4b3536334a46a5c86f57146fd115c7b7b4b5d2bf2de86d",
    "zh:1755c2999e73e4d73f9de670c145c9a0dc5a373802799dff06a0e9c161354163",
    "zh:2b29d706353bc9c4edda6a2946af3322abe94372ffb421d81fa176f1e57e33be",
    "zh:34f65259c6d2bd51582b6da536e782b181b23725782b181193b965f519fbbacd",
    "zh:370f6eb744475926a1fa7464d82d46ad83c2e1148b4b21681b4cec4d75b97969",
    "zh:5950bdb23b4fcc6431562d7eba3dea37844aa4220c4da2eb898ae3e4d1b64ec4",
    "zh:8f3d5c8d4b9d497fec36953a227f80c76d37fc8431b683a23fb1c42b9cccbf8a",
    "zh:8f6eb5e65c047bf490ad3891efecefc488503b65898d4ee106f474697ba257d7",
    "zh:a7040eed688316fe00379574c72bb8c47dbe2638b038bb705647cbf224de8f72",
    "zh:e561f28df04d9e51b75f33004b7767a53c45ad96e3375d86181ba1363bffbc77",
  ]
}

provider "registry.opentofu.org/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:tdS0otiAtvUV8uLJWJNfcqOPo3llj7FyRzExw6X1srY=",
    "zh:374c28bafc43cd65e578cb209efc9eee4c1cec7618f451528e928db98059e8c8",
    "zh:6a2982e70fbc2ab2668d624c648ef2eb32243c1a1185246b03991a7a21326db9",
    "zh:af83169c21bb13f141510a349e1f70cf7d893247a269bd71cad74dd22f1df0f5",
    "zh:b81a5bedc91a1a81b938c393247248d6c3d1bd8ea685541f9c858908c0afb6b3",
    "zh:de15486244af2d29d44d510d647cd6e0b1408e89952261013c572b7c9bfd744b",
  ]
}
