resource "aws_sqs_queue" "queue" {
  name                      = "apigateway-queue"
  delay_seconds             = 0
  max_message_size          = 262144
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  kms_master_key_id         = aws_api_gateway_api_key.mykey.id

  tags = {
    Product   = var.app_name
    yor_trace = "03097263-8895-461a-b621-347efdf2a503"
  }
}


# Trigger lambda on message to SQS
resource "aws_lambda_event_source_mapping" "event_source_mapping" {
  batch_size       = 1
  event_source_arn = aws_sqs_queue.queue.arn
  enabled          = true
  function_name    = var.function_name
}

resource "aws_sqs_queue" "i2c_webhook_routing_error_queue" {
  name = "i2c-webhook-routing-error-queue"
  # Other queue properties...
}
