# AWS authentication variables
variable "aws_access_key" {
  type        = string
  description = "AWS Access Key"
}

variable "aws_secret_key" {
  type        = string
  description = "AWS Secret Key"
}

variable "aws_account_id" {
  type        = string
  description = "AWS Account ID"
}

# Define application name
variable "app_name" {
  type        = string
  description = "Name of the application"
}

# AWS Region
variable "region" {
  type        = string
  description = "AWS Region"
  default     = "us-east-1"
}

# Lambda function variables
variable "function_name" {
  type = string
}

variable "handler" {
  type = string
}

variable "log_bucket_name" {
  type = string
}

variable "lambda_count" {
  type = number
}

variable "name" {
  type        = string
  description = "Name of the resource"
}

variable "stage_name" {
  type        = string
  description = "Name of the stage"
}

variable "runtime" {
  type = string
}

variable "source_path" {
  type        = string
  description = "The absolute path to a local file or directory containing your Lambda source code"
}

variable "memory_size" {
  type    = number
  default = null
}

variable "timeout" {
  type    = number
  default = 3
}

variable "layers" {
  type    = list(string)
  default = null
}

variable "kms_key_arn" {
  type    = string
  default = null
}

variable "reserved_concurrent_executions" {
  type    = number
  default = null
}

variable "description" {
  type    = string
  default = null
}

variable "tags" {
  type    = map(string)
  default = null
}



variable "vpc_config" {
  type = object({
    security_group_ids = list(string)
    subnet_ids         = list(string)
  })
  default = null
}

variable "dead_letter_config" {
  type = object({
    target_arn = string
  })
  default = null
}

variable "tracing_config" {
  type = object({
    mode = string
  })
  default = {
    mode = "Active"
  }
}

variable "enabled" {
  description = "Enable or disable the Lambda resources."
  type        = bool
  default     = true
}

variable "s3_bucket_name" {
  type        = string
  description = "Name of the S3 bucket"
  default     = "my-s3-bucket"
}

variable "lambda_sqs_name" {
  description = "Name for the Lambda SQS"
  default     = "sqs"
}

variable "lambda_api_gateway_name" {
  description = "Name for the Lambda API Gateway"
  default     = "api_gateway"
}

variable "api_gateway_name" {
  description = "Name for the API Gateway"
  default     = "api-gateway"
}

variable "api" {
  type = object({
    name                  = string
    path                  = string
    method                = string
    cors                  = string
    authorizer            = string
    integration           = string
    request_parameters    = map(string)
    request_templates     = map(string)
    responses             = map(string)
    response_parameters   = map(string)
    response_templates    = map(string)
    cache_key_parameters  = list(string)
    cache_namespace       = string
    cache_ttl             = number
    content_handling      = string
    content_type          = string
    credentials           = string
    integration_responses = map(string)
    passthrough_behavior  = string
    timeout_milliseconds  = number
    to_header             = string
    to_querystring        = string
    uri                   = string
    uri_parameters        = map(string)
  })
  default = null
}

variable "lambda_at_edge" {
  description = "Set this to true if using Lambda@Edge, to enable publishing, limit the timeout, and allow edgelambda.amazonaws.com to invoke the function"
  type        = bool
  default     = false
}

variable "policy" {
  description = "An additional policy to attach to the Lambda function role"
  type = object({
    json = string
  })
  default = null
}

variable "trusted_entities" {
  description = "Lambda function additional trusted entities for assuming roles (trust relationship)"
  type        = list(string)
  default     = []
}

variable "cloudwatch_logs" {
  description = "Set this to false to disable logging your Lambda output to CloudWatch Logs"
  type        = bool
  default     = true
}

variable "lambda_sqs" {
  description = "Name for the Lambda SQS"
  default     = "sqs"
}

variable "lambda_api_gateway" {
  description = "Name for the Lambda API Gateway"
  default     = "api_gateway"
}

variable "api_gateway" {
  description = "Name for the API Gateway"
  default     = "api-gateway"
}

variable "vpc_link" {
  type    = string
  default = null
}

variable "publish" {
  type    = bool
  default = false
}

variable "build_command" {
  description = "The command to run to create the Lambda package zip file"
  type        = string
  default     = "python build.py '$filename' '$runtime' '$source'"
}

variable "build_paths" {
  description = "The files or directories used by the build command, to trigger new Lambda package builds whenever build scripts change"
  type        = list(string)
  default     = ["build.py"]
}

variable "path_module" {
  description = "The absolute path to a local file or directory containing your Lambda source code"
  type        = string
  default     = "value"
}

variable "queue_name" {
  description = "The name of the SQS queue to create"
  type        = string
  default     = "value"
}
