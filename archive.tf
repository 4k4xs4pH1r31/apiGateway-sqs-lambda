# Generates a filename for the zip archive based on the contents of the files
# in source_path. The filename will change when the source code changes.
data "external" "archive" {
  count = var.enabled ? 1 : 0

  program = [
    path.module == "." ? "python" : "${path.module}/python", 
    "${var.path_module}/hash.py"
  ]

  query = {
    build_command  = var.build_command
    build_paths    = jsonencode(var.build_paths)
    module_relpath = var.path_module
    runtime        = var.runtime
    source_path    = var.source_path
  }
}

# Build the zip archive whenever the filename changes.
resource "null_resource" "archive" {
  count = var.enabled ? 1 : 0

  triggers = {
    filename = data.external.archive[0].result["filename"]
  }

  provisioner "local-exec" {
    command     = data.external.archive[0].result["build_command"]
    working_dir = var.path_module
  }
}

# Check that the null_resource.archive file has been built. This will rebuild
# it if missing. This is used to catch situations where the Terraform state
# does not match the Lambda function in AWS, e.g. after someone manually
# deletes the Lambda function. If the file is rebuilt here, the build
# output is unfortunately invisible.
data "external" "built" {
  count = var.enabled ? 1 : 0

  program = [
    path.module == "." ? "python" : "${path.module}/python",
    "${var.path_module}/built.py"
  ]

  query = {
    build_command  = data.external.archive[0].result["build_command"]
    filename_old   = null_resource.archive[0].triggers["filename"]
    filename_new   = data.external.archive[0].result["filename"]
    module_relpath = path.module
  }
}
