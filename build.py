"""
This module contains functions for performing file and directory operations,
as well as encryption and interaction with AWS Secrets Manager. It provides
utilities for copying files, create ZIP, generating random passwords,
encrypting passwords using RSA encryption, storing and retrieving encrypted
passwords from AWS Secrets Manager, and more.

The main entry point of this module is the `main` function, which executes the
build process. It takes several parameters for configuring the operation. See
the example usage in the `if __name__ == '__main__':` block for details on how
to use the `main` function.
"""

import base64
import os
import shlex
import shutil
import subprocess
import sys
import tempfile
import zipfile
from contextlib import contextmanager

import boto3
import rsa


@contextmanager
def change_directory(path):
    """
    Changes the working directory.
    """
    sanitized_path = os.path.abspath(path)
    if not os.path.isdir(sanitized_path):
        print("Invalid directory path:", sanitized_path)
        sys.exit(1)
    print('cd', sanitized_path)
    previous_dir = os.getcwd()
    os.chdir(sanitized_path)
    try:
        yield
    finally:
        print('cd', previous_dir)
        os.chdir(previous_dir)


def copy_files(source_dir, destination_dir):
    """
    Copies files from the source directory to the destination directory.
    """
    sanitized_source_dir = os.path.abspath(source_dir)
    sanitized_destination_dir = os.path.abspath(destination_dir)

    for root, _, files in os.walk(sanitized_source_dir):
        for file_name in files:
            source_path = os.path.join(root, file_name)
            relative_path = os.path.relpath(source_path, sanitized_source_dir)
            destination_path = os.path.abspath(
                os.path.join(sanitized_destination_dir, relative_path)
            )

            # Ensure the destination path is within the destination directory
            destination_paths = [sanitized_destination_dir, destination_path]
            common_path = os.path.commonpath(destination_paths)
            if not common_path == sanitized_destination_dir:
                raise ValueError('Invalid destination path')

            print('cp', source_path, destination_path)
            shutil.copy2(source_path, destination_path)


def build_zip_file(source_dir, target_file):
    """
    Builds a zip file from a directory or file.

    Args:
        source_dir: The directory or file to zip.
        target_file: The path to the zip file to create.

    Returns:
        None.
    """
    sanitized_source_dir = os.path.abspath(source_dir)  # Sanitize input
    sanitized_target_file = os.path.abspath(target_file)  # Sanitize input

    if not os.path.exists(sanitized_source_dir):
        raise FileNotFoundError('The source directory does not exist.')

    try:
        with zipfile.ZipFile(
            sanitized_target_file,
            'w',
            compression=zipfile.ZIP_DEFLATED
        ) as zip_file:
            for root, _, files in os.walk(sanitized_source_dir):
                for file in files:
                    file_path = os.path.join(root, file)
                    relative_path = os.path.relpath(
                        file_path,
                        sanitized_source_dir
                    )
                    print('Adding to zip:', relative_path)
                    zip_file.write(file_path, arcname=relative_path)
    except subprocess.CalledProcessError as error:
        print('Error occurred while executing the command:', error)
        sys.exit(1)


def format_command(command):
    """
    Formats a command for displaying on the screen.
    """
    return ' '.join(map(shlex.quote, command))


def list_files(top_path):
    """
    Returns a sorted list of all files in a directory.
    """
    results = []

    sanitized_top_path = os.path.abspath(top_path)
    if not os.path.isdir(sanitized_top_path):
        print("Invalid directory path:", sanitized_top_path)
        sys.exit(1)

    for root, _, files in os.walk(sanitized_top_path):
        if root.endswith("__pycache__"):
            continue
        for file_name in files:
            file_path = os.path.join(root, file_name)
            relative_path = os.path.relpath(file_path, sanitized_top_path)
            results.append(relative_path)

    results.sort()
    return results


def run(*args, **kwargs):
    """
    Runs a command and prints the command being executed.
    """
    command_str = format_command(args)
    print('Executing:', command_str)
    try:
        subprocess.run(args, check=True, **kwargs)
    except FileNotFoundError as exception:
        # Exception handling code for FileNotFoundError
        print('Error occurred while building the zip file:', str(exception))
        sys.exit(1)


@contextmanager
def tempdir():
    """
    Creates a temporary directory and deletes it afterwards.
    """
    temp_dir = tempfile.mkdtemp()
    try:
        yield temp_dir
    finally:
        shutil.rmtree(temp_dir)


def create_zip_file(source_dir, target_file):
    """
    Creates a zip file from a directory.

    Args:
        source_dir: The directory to zip.
        target_file: The path to the zip file to create.

    Returns:
        None.
    """
    print("Creating zip file:", target_file)
    shutil.make_archive(target_file, 'zip', source_dir)


def generate_random_password(length=16):
    """
    Generates a random password.

    Args:
        length: The length of the password. Default is 16.

    Returns:
        The generated password as a string.
    """
    password_bytes = rsa.randnum.read_random_bits(length)
    password = base64.urlsafe_b64encode(password_bytes).decode('utf-8')
    return password


def encrypt_password(password, public_key_file):
    """
    Encrypts a password using RSA encryption.

    Args:
        password: The password to encrypt.
        public_key_file: The path to the recipient's public key file.

    Returns:
        The encrypted password as a string.
    """
    with open(public_key_file, 'rb') as key_file:
        public_key = rsa.PublicKey.load_pkcs1_openssl_pem(key_file.read())
    encrypted_password = rsa.encrypt(password.encode('utf-8'), public_key)
    return encrypted_password


def store_encrypted_password(encrypted_password, secret_name):
    """
    Stores the encrypted password in AWS Secrets Manager.

    Args:
        encrypted_password: The encrypted password to store.
        secret_name: The name of the secret.

    Returns:
        None.
    """
    client = boto3.client('secretsmanager')
    print('Storing encrypted password in Secrets Manager:', secret_name)
    response = client.create_secret(
        Name=secret_name, SecretBinary=encrypted_password)
    print('Secret ARN:', response['ARN'])


def retrieve_encrypted_password(secret_name):
    """
    Retrieves the encrypted password from AWS Secrets Manager.

    Args:
        secret_name: The name of the secret.

    Returns:
        The encrypted password as a string.
    """
    client = boto3.client('secretsmanager')
    print('Retrieving encrypted password from Secrets Manager:', secret_name)
    response = client.get_secret_value(SecretId=secret_name)
    encrypted_password = response['SecretBinary']
    return encrypted_password


def main(source_dir_param, destination_dir_param, public_key_file_param,
         secret_name_param, zip_file_name_param):
    """
    The main function that executes the build process.

    Args:
        source_dir_param: The source directory containing the files to copy.
        destination_dir_param: The destination directory to copy the files to.
        public_key_file_param: The path to the recipient's public key file.
        secret_name_param: The name of the secret in AWS Secrets Manager.
        zip_file_name_param: The name of the zip file to create.

    Returns:
        None.
    """
    copy_files(source_dir_param, destination_dir_param)

    with tempdir() as temp_dir:
        build_zip_file(destination_dir_param, os.path.join(
            temp_dir, zip_file_name_param))

        with change_directory(temp_dir):
            run('git', 'init')
            run('git', 'add', '.')
            run('git', 'commit', '-m', 'Initial commit')

            random_password = generate_random_password()
            encrypted_password = encrypt_password(
                random_password, public_key_file_param)
            store_encrypted_password(encrypted_password, secret_name_param)

            run('git', 'remote', 'add', 'origin', 'https://example.com')
            run('git', 'push', '-u', 'origin', 'master')

    encrypted_password = retrieve_encrypted_password(secret_name_param)
    print('Encrypted password:', encrypted_password.decode('utf-8'))


if __name__ == '__main__':
    # Example usage:
    main('source_dir', 'destination_dir', 'public_key_file.pem',
         'secret_name', 'zip_file.zip')
