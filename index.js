const AWS = require('aws-sdk');

const sqs = new AWS.SQS({
  region: 'us-east-1',
});

exports.handler = async (event) => {
  // Parse the event payload.
  const eventPayload = JSON.parse(event.body);

  // Check if the event is a valid SQS message.
  if (!eventPayload.Records || !eventPayload.Records[0]) {
    return;
  }

  // Get the SQS message from the event.
  const message = eventPayload.Records[0].body;

  // Send the SQS message to a queue.
  await sqs.sendMessage({
    QueueUrl: process.env.QUEUE_URL,
    MessageBody: message,
  });
};
