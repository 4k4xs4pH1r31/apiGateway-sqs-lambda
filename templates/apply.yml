spec:
  inputs:
    # Job and Stage name
    as:
      default: 'apply'
      description: 'Defines the name of this job.'
    stage:
      default: 'deploy'
      description: 'Defines the stage that this job will belong to.'

    # Versions
    # This version is only required, because we cannot access the context of the component,
    # see https://gitlab.com/gitlab-org/gitlab/-/issues/438275
    version:
      default: 'latest'
      description: 'Version of this component. Has to be the same as the one in the component include entry.'

    opentofu_version:
      default: '1.6.2'
      options:
        - '$OPENTOFU_VERSION'
        - '1.7.0-alpha1'
        - '1.6.2'
        - '1.6.1'
        - '1.6.0'
      description: 'OpenTofu version that should be used.'

    # Images
    image_registry_base:
      default: '$CI_REGISTRY/components/opentofu'
      description: 'Host URI to the job images. Will be combined with `image_name` to construct the actual image URI.'
    # FIXME: not yet possible because of https://gitlab.com/gitlab-org/gitlab/-/issues/438722
    # gitlab_opentofu_image:
    #   # FIXME: This should reference the component tag that is used.
    #   #        Currently, blocked by https://gitlab.com/gitlab-org/gitlab/-/issues/438275
    #   # default: '$CI_REGISTRY/components/opentofu/gitlab-opentofu:$[[ inputs.opentofu_version ]]'
    #   default: '$CI_REGISTRY/components/opentofu/gitlab-opentofu:$[[ inputs.version ]]-opentofu$[[ inputs.opentofu_version ]]'
    #   description: 'Tag of the gitlab-opentofu image.'

    image_name:
      default: 'gitlab-opentofu'
      description: 'Image name for the job images. Hosted under `image_registry_base`.'

    # Configuration
    root_dir:
      default: ${CI_PROJECT_DIR}
      description: 'Root directory for the OpenTofu project.'
    state_name:
      default: default
      description: 'Remote OpenTofu state name.'
    auto_apply:
      default: false
      type: boolean
      description: 'Whether the apply job is manual or automatically run.'

---

'$[[ inputs.as ]]':
  stage: $[[ inputs.stage ]]
  environment:
    name: $TF_STATE_NAME
    action: start
  resource_group: $TF_STATE_NAME
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && "$[[ inputs.auto_apply ]]" == "true"'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
  cache:
    key: "$__CACHE_KEY_HACK"
    paths:
      - $TF_ROOT/.terraform/
  variables:
    # FIXME: work around to make slashes work in `cache:key`. see https://gitlab.com/gitlab-org/gitlab/-/issues/439898
    __CACHE_KEY_HACK: "$[[ inputs.root_dir ]]"
    TF_ROOT: $[[ inputs.root_dir ]]
    TF_STATE_NAME: $[[ inputs.state_name ]]
  image:
    name: '$[[ inputs.image_registry_base ]]/$[[ inputs.image_name ]]:$[[ inputs.version ]]-opentofu$[[ inputs.opentofu_version ]]'
  script:
    - gitlab-tofu apply
