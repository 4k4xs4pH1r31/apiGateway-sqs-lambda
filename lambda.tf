resource "aws_s3_bucket" "log_bucket" {
  bucket = var.log_bucket_name
}

resource "aws_s3_bucket_versioning" "log_bucket_versioning" {
  bucket = aws_s3_bucket.log_bucket.id

  versioning_configuration {
    status = "Enabled"
    mfa_delete = "Enabled"
  }
}

resource "aws_s3_bucket_logging" "log_bucket_logging_target" {
  bucket = var.log_bucket_name

  target_bucket = aws_s3_bucket.log_bucket.bucket
  target_prefix = "logs/"
}

resource "aws_iam_user" "mfa_user" {
  name = "s3-mfa-user"
}

resource "aws_iam_virtual_mfa_device" "mfa_device" {
  virtual_mfa_device_name = "s3-mfa-device"

  tags = {
    Name = "s3-mfa-device"
  }
}

resource "aws_iam_group" "mfa_group" {
  name = "s3-mfa-group"
}

resource "aws_iam_group_policy_attachment" "mfa_group_attachment" {
  group      = aws_iam_group.mfa_group.name
  policy_arn = aws_iam_policy.mfa_policy.arn
}

resource "aws_iam_user_group_membership" "mfa_user_membership" {
  user   = aws_iam_user.mfa_user.name
  groups = [aws_iam_group.mfa_group.name]
}

resource "aws_iam_policy" "mfa_policy" {
  name = "s3-mfa-policy"
  policy = jsonencode({
    Version   = "2012-10-17"
    Statement = [{
      Sid    = "EnforceMFAOnDelete"
      Effect = "Deny"
      Action = [
        "s3:DeleteBucket",
        "s3:DeleteBucketPolicy",
        "s3:DeleteBucketWebsite",
      ]
      Resource  = aws_s3_bucket.log_bucket.arn
      Condition = {
        Bool = {
          "aws:MultiFactorAuthPresent" = "false"
        }
      }
    }]
  })
}

resource "aws_lambda_permission" "allows_sqs_to_trigger_lambda" {
  count          = var.lambda_count
  statement_id   = "AllowExecutionFromSQS_${count.index}"
  action         = "lambda:InvokeFunction"
  function_name  = aws_lambda_function.lambda[count.index].function_name
  principal      = "sqs.amazonaws.com"
  source_arn     = aws_sqs_queue.queue.arn
  source_account = var.aws_account_id
}

resource "aws_lambda_function" "lambda" {
  count                          = var.enabled ? 1 : 0
  function_name                  = var.function_name
  description                    = var.description
  role                           = aws_iam_role.lambda[0].arn
  handler                        = var.handler
  memory_size                    = var.memory_size
  reserved_concurrent_executions = var.reserved_concurrent_executions
  runtime                        = var.runtime
  timeout                        = var.timeout
  publish                        = var.publish

  filename   = data.external.built[0].result.filename
  depends_on = [null_resource.archive]

  tracing_config {
    mode = "Active"
  }

  dynamic "dead_letter_config" {
    for_each = var.dead_letter_config != null ? [var.dead_letter_config] : []
    content {
      target_arn = dead_letter_config.value.target_arn
    }
  }

  dynamic "vpc_config" {
    for_each = var.vpc_config != null ? [var.vpc_config] : []
    content {
      security_group_ids = vpc_config.value.security_group_ids
      subnet_ids         = vpc_config.value.subnet_ids
    }
  }

  dynamic "tracing_config" {
    for_each = var.tracing_config != null ? [var.tracing_config] : []
    content {
      mode = tracing_config.value.mode
    }
  }
}
