"""
Module docstring: Provide a brief overview of the module here.
"""

import json
import os
import subprocess
import sys


def main():
    """
    Function docstring: Describe what this function does.
    """
    # Parse the query.
    query = json.load(sys.stdin)
    build_command = query['build_command']
    filename_old = query['filename_old']
    filename_new = query['filename_new']
    module_relpath = query['module_relpath']

    # If old filename (from the Terraform state) matches the new filename
    # (hash.py), then the source code has not changed and thus the zip file
    # should not have changed.
    if filename_old == filename_new:
        if os.path.exists(filename_new):
            # Update the file time so it doesn't get cleaned up,
            # which would result in an unnecessary rebuild.
            os.utime(filename_new, None)
        else:
            # If file is missing, then it was probably generated on another
            # machine, or was created long time ago and cleaned up. This is
            # expected behavior. However, if Terraform needs upload the file
            # (e.g., someone manually deleted Lambda function via the AWS
            # console), then is possible that Terraform will try to upload
            # the missing file. Don't know how to tell if Terraform is going
            # to upload the file or not, so always ensure the file exists.
            subprocess.check_output(
                build_command,
                shell=True,
                cwd=module_relpath
            )

    # Output the filename to Terraform.
    json.dump({
        'filename': os.path.join(module_relpath, filename_new),
    }, sys.stdout, indent=2)
    sys.stdout.write('\n')


if __name__ == "__main__":
    main()
